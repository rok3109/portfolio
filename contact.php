<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Rohan Kusre // Portfolio // UX.UI.Branding.</title>




<!-- Foundation 3 for IE 8 and earlier -->
<!--[if lt IE 9]>
  <link rel="stylesheet" href="css/foundation3/normalize.css">
  <link rel="stylesheet" href="css/foundation3/foundation.css">
  <link rel="stylesheet" href="css/foundation3/app.css">
<![endif]-->

<!-- Foundation 4 for IE 9 and earlier -->
<!--[if gt IE 8]><!-->
  <link rel="stylesheet" href="css/foundation.css">
<!--<![endif]-->


  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="css/style.css">

  <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css">
  <!<![endif]-->

  <!-- Modernizr -->
  <script src="js/modernizr.custom.js"></script>
  <script type="text/javascript" src="js/vendor/jquery.js"></script>

   <!-- Google Analytics -->
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64266915-1', 'auto');
  ga('send', 'pageview');

  </script>

  <!-- IE 10 -->
  <script> 
    if (/*@cc_on!@*/false) { 
        document.documentElement.className+=' ie10'; 
    } 
  </script>

  <!---favicon-->
 <link rel="shortcut icon" type="image/png" href="images/favicon.ico"/>
 <link rel="shortcut icon" type="image/png" href="http://rohankusre.design"/>




</head>
<body class="single-page">




<!-- Briefcase Header -->
<header class="menu-wrap">
    <div class="bc-nav cf">
    
    <!-- Logo -->
    <div class="logo">
        <a href="index.html"><h1 class="uppercase">Rohan Kusre</h1></a>
    </div>


    <!-- Tagline -->
    <p class="nav-tagline" style='color: #000'>UX/UI/Branding</p>

    <!-- Nav -->
    <a href="#menu" class="menu-link"></a>
        <nav id="menu" class="menu" role="navigation">
          <ul class="level-1">
             <li><a href="about.html">About</a>
            </li>
            <li><a href="work.html">Work</a>
            </li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
    
    </div><!-- End briefcase navigation -->
  </header> 
  <!-- Briefcase Header --> 


<!-- Page -->

  <div class="page">
    

    <div class="row page-wrapper">
    <!-- Page Title -->
    <div class="page-title-wrapper">
      <h1 class="page-title-resume uppercase">contact</h1>
      <h1 class="page-title-shadow-resume uppercase">contact</h1>
    </div>
    </div>

  <!-- Form Content -->
  <div class="large-8 eight columns">
    <div class="contact-container"> 

      <div id="contactResponse"></div>
      <?php
        if($_GET['success']=='true'){
          echo "<h2>Successfully sent message!</h2>";
        }
      ?>
      <form method="post" action="php/send.php">

          <!-- Name -->
         <div class="row mb10">
          <label class="desc three large-3 medium-3 small-12 columns" id="title1" for="name"><span>Your name</span></label>
          <div class="nine large-9 medium-9 small-12 columns">
            <input id="Field1" name="name" type="text"  class="field text fn" value="" size="8" tabindex="1">
          </div>
        </div>
          
         <!-- Email --> 
        <div class="row mb10">
          <label  class="desc three large-3 medium-3 small-12 columns" id="title3" for="email"><span>Your email</span></label>
          <div class="nine large-9 medium-9 small-12 columns">
            <input id="Field3" name="email" type="email" spellcheck="false" value="" maxlength="255" tabindex="3"> 
         </div>
        </div>
          


        <!-- Message -->
          <div class="row">
          <label class="desc three large-3 medium-3 small-12 columns" id="title4" for="message"><span>Message</span></label>
        
          <div  class="nine large-9 medium-9 small-12 columns">
            <textarea id="Field4" name="message" spellcheck="true" rows="50" cols="50" tabindex="4"></textarea>
          </div>
        </div>
          

        <a class="btn black contact-form-send"><input class="send-imput" type="submit" value="SEND"/></a>

        
      </form>

 

     </div><!-- Contact Container -->
    </div><!-- Form Content Ends -->



    </div>
  </div><!-- Page Ends -->

       

                
    
     <!-- Footer  -->
            
      <div class="row" id="footer">       

        <div class="twelve large-12 columns">
              <br>
              <br>
              <br>

              <p id='copyright'>
                Rohan Kusre © 2015
              </p>
        </div>
                
      </div>
   

<script>
     $("#contactForm").submit(function(event) 
     {
         /* stop form from submitting normally */
         event.preventDefault();


         /* get some values from elements on the page: */
         var $form = $( this ),
             $submit = $form.find( 'button[type="submit"]' ),
             name_value = $form.find( 'input[name="Field1"]' ).val(),
             email_value = $form.find( 'input[name="Field3"]' ).val(),
             interest_value = $form.find( 'input[name="Field5"]:checked' ).val(),
             message_value = $form.find( 'textarea[name="Field4"]' ).val(),
             url = $form.attr('action');

         /* Send the data using post */
         var posting = $.post( url, { 
                           name: name_value, 
                           email: email_value, 
                           interest: interest_value,
                           message: message_value 
                       });

         posting.done(function( data )
         {
             /* Put the results in a div */
             $( "#contactResponse" ).html(data);

             /* Change the button text. */
             $submit.text('Sent, Thank you');

             /* Disable the button. */
             $submit.attr("disabled", true);
         });
    });
</script>



      <!-- #Footer Ends -->
    


  <!-- Foundation 3 for IE 8 and earlier -->
  <!--[if lt IE 9]>
      <script src="js/excanvas.compiled.js"></script>
      <script src="js/foundation3/foundation.min.js"></script>
      <script src="js/foundation3/app.js"></script>
  <![endif]-->

  <!-- Foundation 4 for IE 9 and later -->
  <!--[if gt IE 8]><!-->
      <script src="js/foundation.min.js"></script>
      <script>
        $(document).foundation();
      </script>  
  <!--<![endif]-->

  
  <script src="js/fittext.js"></script>
  <script src="js/breifcase-functions.js"></script>




      

</body>
</html>